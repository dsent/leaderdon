import 'jquery';
import SimpleBar from 'simplebar';
import 'slick-carousel';
import * as FilePond from 'filepond';
import FilePondPluginFileEncode from 'filepond-plugin-file-encode';
import 'air-datepicker';
import Swal from 'sweetalert2';

$(function() {
    $('.svg-load').each(function(){
        var $img = $(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');

        $.get(imgURL, function(data) {
            var $svg = $(data).find('svg');
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }
            $svg = $svg.removeAttr('xmlns:a');
            $img.replaceWith($svg);
        }, 'xml');
    });

    if($('.scrollbar').length) {
        new SimpleBar($('.scrollbar')[0]);
    }

    $('.forms-block__select').each(function() {
       $(this).find('option').first().text($(this).data('option'));
    });

    $('.dialog-data').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true,
        variableWidth: true
    });

    $('.dialog-we-help-carousel').slick({
        infinite: false,
        speed: 300,
        variableWidth: true
    });

    $('.lskd-target-carousel').slick({
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        variableWidth: true
    });

    $('.menu-button').on('click', function() {
        $('.modal-overlay').show();
        $('.modal-menu').show();
    });

    $('.open-menu-contacts').on('click', function(e) {
        e.preventDefault();
        $('.modal-contacts').show();
    });

    $('.modal-close').on('click', function() {
        $('.modal-overlay').hide();
        $('.modal-menu').hide();
        $('.modal-contacts').hide();
    });

    $('.modal-overlay').on('click', function() {
        $('.modal-overlay').hide();
        $('.modal-menu').hide();
        $('.modal-contacts').hide();
    });

    $('.header__scroll').on('click', function() {
        var headerHeight = $('.header').outerHeight();
        $("html, body").animate({ scrollTop: headerHeight }, "slow");
    });

    FilePond.registerPlugin(FilePondPluginFileEncode);

    const inputElement = document.querySelector('.input-file');
    const pond = FilePond.create(inputElement);
    pond.setOptions({
        labelIdle: 'Загрузить еще',
        styleButtonRemoveItemPosition: 'right'
    });

    $('.checkbox-list').each(function() {
        new CheckBoxCountValidate($(this))
    });
    
    $('.header__menu').on('click', 'a', function () {
        $('html, body').animate({
            scrollTop: $($(this).attr('href')).offset().top
        }, 2000);
    });

    function CheckBoxCountValidate(list) {
        var count = 0;
        var max = list.data('max');

        list.on('change', 'input', function(e) {
            if($(this).prop("checked")) {
                if(count === max) {
                    $(this).prop("checked", false);

                    Swal.fire({
                        toast: true,
                        icon: 'warning',
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000,
                        title: 'Можно выбрать только ' + max + ' значения'
                    })
                } else {
                    ++count;
                }
            } else {
                --count;
            }
        });
    }
});
